all: clean roles

.PHONY: roles
roles:
		git clean -fdx roles
		ansible-galaxy install -r roles/requirements.yml

.PHONY: clean
clean:
		git clean -fdx
		git reset --hard